#### Project setup
  * Run Composer create network command from the console
    * `docker network create coding`
  * Run Composer up command
    * `docker-compose up -d`
    
#### API Documentation
  * You can find all API calls here
    * `http://localhost:18030/api/docs`